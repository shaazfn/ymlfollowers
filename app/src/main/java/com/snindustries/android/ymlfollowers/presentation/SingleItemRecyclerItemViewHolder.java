package com.snindustries.android.ymlfollowers.presentation;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import com.snindustries.android.ymlfollowers.BR;

/**
 * A view holder that uses databinding to a single variable.
 */
public class SingleItemRecyclerItemViewHolder extends RecyclerView.ViewHolder {

	private final ViewDataBinding binding;

	public SingleItemRecyclerItemViewHolder(ViewDataBinding binding) {
		super(binding.getRoot());
		this.binding = binding;
	}

	public void bind(Object item) {
		binding.setVariable(BR.item, item);
		binding.executePendingBindings();
	}

}
