package com.snindustries.android.ymlfollowers;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.snindustries.android.ymlfollowers.databinding.ActivityUserBinding;
import com.snindustries.android.ymlfollowers.model.FollowerViewModel;
import com.snindustries.android.ymlfollowers.model.UserViewModel;
import com.snindustries.android.ymlfollowers.presentation.BasicViewModel;
import com.snindustries.android.ymlfollowers.services.GitHubResultReceiver;
import com.snindustries.android.ymlfollowers.services.GitHubService;

import org.json.JSONException;

/**
 * Displays a user's profile.
 * <p>
 * Created by shaaz on 3/10/2017.
 */
public class UserActivity extends AppCompatActivity implements GitHubResultReceiver.Receiver, BasicViewModel.OnClickListener {
    public static final String EXTRA_FOLLOWER_MODEL = "EXTRA_FOLLOWER_MODEL";
    ActivityUserBinding views;
    private GitHubResultReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        views = DataBindingUtil.setContentView(this, R.layout.activity_user);
        setSupportActionBar(views.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        receiver = new GitHubResultReceiver(new Handler());
        receiver.setReceiver(this);
        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        if (intent != null) {
            FollowerViewModel follower = (FollowerViewModel) intent.getSerializableExtra(EXTRA_FOLLOWER_MODEL);
            if (follower != null) {
                requestUserProfile(follower.getLogin());
            }
        }
    }

    private void requestUserProfile(String user) {
        Intent intent = new Intent(Intent.ACTION_SYNC, null, this, GitHubService.class);
        intent.putExtra(GitHubService.EXTRA_USER_ID, user);
        intent.putExtra(GitHubService.EXTRA_RESULT_RECEIVER, receiver);
        intent.putExtra(GitHubService.EXTRA_SERVICE_ENDPOINT, GitHubService.ENDPOINT_USER_PROFILE);
        startService(intent);
    }

    @Override
    public void onFollowersDownloaded(String resultData) {
        //Do nothing
    }

    @Override
    public void onProfileDownloaded(String resultData) {
        try {
            UserViewModel userViewModel = new UserViewModel(this, resultData);
            views.setItem(userViewModel);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

    }

    @Override
    public void onClicked(View view, BasicViewModel listingViewModel) {
        //Do Nothing
    }
}
