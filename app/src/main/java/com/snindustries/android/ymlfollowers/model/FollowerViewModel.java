package com.snindustries.android.ymlfollowers.model;

import android.databinding.Bindable;

import com.snindustries.android.ymlfollowers.presentation.BasicViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * View Model class for followers.
 * Created by shaaz on 3/10/2017.
 */
public class FollowerViewModel extends BasicViewModel implements Serializable {
    //TODO: change the class to Pareclable for performance if required

    private String avatarUrl;
    private String login;

    public FollowerViewModel() {
        super(null);
        avatarUrl = "";
        login = "";
    }

    public FollowerViewModel(BasicViewModel.OnClickListener listener, String json) throws JSONException {
        this(listener, new JSONObject(json));
    }

    public FollowerViewModel(BasicViewModel.OnClickListener listener, JSONObject json) throws JSONException {
        super(listener);
        this.avatarUrl = json.getString("avatar_url");
        this.login = json.getString("login");
    }

    @Bindable
    public String getAvatarUrl() {
        return avatarUrl;
    }


    @Bindable
    public String getLogin() {
        return login;
    }

}
