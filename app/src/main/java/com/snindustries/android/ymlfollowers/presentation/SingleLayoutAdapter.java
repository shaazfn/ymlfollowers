package com.snindustries.android.ymlfollowers.presentation;

import android.support.annotation.LayoutRes;

/**
 * Adapter with a single type of layout.
 */
public abstract class SingleLayoutAdapter extends BaseRecyclerAdapter {

	private final int layoutId;

	public SingleLayoutAdapter(@LayoutRes int layoutId) {
		this.layoutId = layoutId;
	}

	@Override
	protected int getLayoutIdForPosition(int position) {
		return layoutId;
	}
}
