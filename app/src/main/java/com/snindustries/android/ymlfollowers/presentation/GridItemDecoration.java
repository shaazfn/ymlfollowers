package com.snindustries.android.ymlfollowers.presentation;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

/**
 * Add internal spaces between items in the grid.
 * Created by shaaz on 3/10/2017.
 */
public class GridItemDecoration extends RecyclerView.ItemDecoration {

    private final int space;
    private final int spanCount;

    public GridItemDecoration(int space, int spanCount) {
        this.space = space;
        this.spanCount = spanCount;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, State state) {
        int itemPosition = parent.getChildLayoutPosition(view);
        //If this is the first item, add margin on the top
        outRect.top = itemPosition / spanCount == 0 ? space : 0;
        //If this is the left most, then add margin to left
        outRect.left = itemPosition % spanCount == 0 ? space : 0;
        //Add margins to right and bottom
        outRect.right = space;
        outRect.bottom = space;
    }
}
