package com.snindustries.android.ymlfollowers.presentation;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 * A base recycler that uses data binding with a single variable.
 * Created by shaaz on 3/10/2017.
 */
public abstract class BaseRecyclerAdapter extends RecyclerView.Adapter<SingleItemRecyclerItemViewHolder> {

    @NonNull
    protected abstract Object getItemForPosition(int position);

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    @LayoutRes
    protected abstract int getLayoutIdForPosition(int position);

    @Override
    public void onBindViewHolder(SingleItemRecyclerItemViewHolder holder, int position) {
        holder.bind(getItemForPosition(position));
    }

    @Override
    public SingleItemRecyclerItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, viewType, parent, false);
        return new SingleItemRecyclerItemViewHolder(binding);
    }
}
