package com.snindustries.android.ymlfollowers.services;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by u1cc16 on 3/10/2017.
 */
public class GitHubResultReceiver extends ResultReceiver {

	public interface Receiver {

		void onFollowersDownloaded(String resultData);//Convenience method

		void onProfileDownloaded(String resultData);//Convenience method

		void onReceiveResult(int resultCode, Bundle resultData);
	}

	private Receiver receiver;

	/**
	 * Create a new ResultReceive to receive results.  Your
	 * {@link #onReceiveResult} method will be called from the thread running
	 * <var>handler</var> if given, or from an arbitrary thread if null.
	 * @param handler
	 */
	public GitHubResultReceiver(Handler handler) {
		super(handler);
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		if (receiver != null) {
			switch (resultCode) {
				case GitHubService.STATUS_FINISHED_USER_PROFILE:
					receiver.onProfileDownloaded(resultData.getString(Intent.EXTRA_TEXT));
					break;
				case GitHubService.STATUS_FINISHED_USER_FOLLOWERS:
					receiver.onFollowersDownloaded(resultData.getString(Intent.EXTRA_TEXT));
					break;
				default:
					receiver.onReceiveResult(resultCode, resultData);
			}
		}
	}

	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}
}
