package com.snindustries.android.ymlfollowers;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Toast;

import com.snindustries.android.ymlfollowers.databinding.ActivityHomeBinding;
import com.snindustries.android.ymlfollowers.model.FollowerViewModel;
import com.snindustries.android.ymlfollowers.model.UserViewModel;
import com.snindustries.android.ymlfollowers.presentation.BasicViewModel;
import com.snindustries.android.ymlfollowers.presentation.GridItemDecoration;
import com.snindustries.android.ymlfollowers.presentation.SingleItemRecyclerAdapter;
import com.snindustries.android.ymlfollowers.services.GitHubResultReceiver;
import com.snindustries.android.ymlfollowers.services.GitHubService;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * The landing page.  Search for a user and then load their followers.
 *
 * Created by shaaz on 3/10/2017.
 */
public class HomeActivity extends AppCompatActivity implements GitHubResultReceiver.Receiver, BasicViewModel.OnClickListener {

    public static final int SPAN_COUNT = 3;
    public static final String EXTRA_USER_MODEL = "EXTRA_USER_MODEL";
    private SingleItemRecyclerAdapter<FollowerViewModel> adapter;
    private GitHubResultReceiver receiver;
    private SearchView searchView;
    private ActivityHomeBinding views;

    private void handleIntent(Intent intent) {
        if (intent == null) {
            return;
        }
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if (searchView != null && searchView.hasFocus()) {
                searchView.clearFocus();
            }
            requestUserProfile(query);
        } else if (intent.getSerializableExtra(EXTRA_USER_MODEL) != null) {
            UserViewModel userVM = (UserViewModel) intent.getSerializableExtra(EXTRA_USER_MODEL);
            setTitle(userVM.getName());
            userVM.setListener(this);
            requestUserFollowers(userVM.getLogin());//TODO cache the list or request new.
        }
    }

    private void initFollowersRecycler() {
        views.followers.setLayoutManager(new GridLayoutManager(this, SPAN_COUNT));
        views.followers.addItemDecoration(new GridItemDecoration(getResources().getDimensionPixelSize(R.dimen.padding), SPAN_COUNT));
        adapter = new SingleItemRecyclerAdapter<>(new ArrayList<FollowerViewModel>(), R.layout.followers_summary_items);
        views.followers.setAdapter(adapter);
    }

    @Override
    public void onClicked(View view, BasicViewModel listingViewModel) {
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra(UserActivity.EXTRA_FOLLOWER_MODEL, (FollowerViewModel) listingViewModel);
        Pair<View, String> pair1 = Pair.create(view.findViewById(R.id.avatar), getResources().getString(R.string.transition_detail_image));
        Pair<View, String> pair2 = Pair.create(view.findViewById(R.id.userName), getResources().getString(R.string.transition_detail_name));
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pair1, pair2);
        startActivity(intent, options.toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        views = DataBindingUtil.setContentView(this, R.layout.activity_home);
        setSupportActionBar(views.toolbar);
        handleIntent(getIntent());
        receiver = new GitHubResultReceiver(new Handler());
        receiver.setReceiver(this);
        initFollowersRecycler();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    public void onFollowersDownloaded(String resultData) {
        dismissSearchView();
        try {
            JSONArray followers = new JSONArray(resultData);
            for (int index = 0; index < followers.length(); index++) {
                adapter.addItem(new FollowerViewModel(this, followers.getJSONObject(index)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void dismissSearchView() {
        //The android API is named wrong
        searchView.onActionViewCollapsed();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    @Override
    public void onProfileDownloaded(String resultData) {
        UserViewModel user = null;
        try {
            user = new UserViewModel(this, resultData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (user != null) {
            setTitle(user.getName());
            Intent intent = new Intent(this, HomeActivity.class);
            intent.putExtra(EXTRA_USER_MODEL, user);
            startActivity(intent);//Can do this because Activity is SingleTop
        } else {
            Snackbar.make(views.getRoot(), "User does not exist", Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case GitHubService.STATUS_STARTED:
                onUserProfileDownloadStarted();
                break;
            case GitHubService.STATUS_ERROR:
                onUserProfileDownloadError();
                break;
            default:
                break;
        }
    }

    private void onUserProfileDownloadError() {
        Snackbar.make(views.getRoot(), "Error Downloading User Profile", Snackbar.LENGTH_LONG).show();
        //TODO dismiss progress dialog
    }

    private void onUserProfileDownloadStarted() {
        //TODO show progress dialog
    }

    private void requestUserFollowers(String user) {
        Intent intent = new Intent(Intent.ACTION_SYNC, null, this, GitHubService.class);
        intent.putExtra(GitHubService.EXTRA_USER_ID, user);
        intent.putExtra(GitHubService.EXTRA_RESULT_RECEIVER, receiver);
        intent.putExtra(GitHubService.EXTRA_SERVICE_ENDPOINT, GitHubService.ENDPOINT_USER_FOLLOWERS);
        startService(intent);
    }

    private void requestUserProfile(String user) {
        Intent intent = new Intent(Intent.ACTION_SYNC, null, this, GitHubService.class);
        intent.putExtra(GitHubService.EXTRA_USER_ID, user);
        intent.putExtra(GitHubService.EXTRA_RESULT_RECEIVER, receiver);
        intent.putExtra(GitHubService.EXTRA_SERVICE_ENDPOINT, GitHubService.ENDPOINT_USER_PROFILE);
        startService(intent);
    }

}
