package com.snindustries.android.ymlfollowers.presentation;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;

import java.util.List;

/**
 * A recycler adapter that uses list to add and remove items.
 *
 * @param <T>
 */
public class SingleItemRecyclerAdapter<T> extends SingleLayoutAdapter {

    private final List<T> list;

    public SingleItemRecyclerAdapter(List<T> list, @LayoutRes int layout) {
        super(layout);
        this.list = list;
    }

    public void addItem(T item) {
        list.add(item);
        notifyItemChanged(list.size() - 1);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @NonNull
    @Override
    protected Object getItemForPosition(int position) {
        return list.get(position);
    }

    public void removeItem(T item) {
        int index = list.indexOf(item);
        if (index >= 0) {
            list.remove(item);
            notifyItemRemoved(index);
        }
    }

}
