package com.snindustries.android.ymlfollowers.presentation;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;

import com.snindustries.android.ymlfollowers.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

/**
 * View Model class used in recycler view via databinding.
 * Created by shaaz on 3/10/2017.
 */
public class BasicViewModel extends BaseObservable {

    public static final Transformation CIRCLE_TRANSFORM = new CircleTransform();
    private transient OnClickListener listener = null;

    public BasicViewModel() {
    }

    public BasicViewModel(OnClickListener listener) {
        this.listener = listener;
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            Picasso.with(view.getContext())
                    .load(imageUrl)
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.broken)
                    .into(view);
        } else {
            Picasso.with(view.getContext())
                    .load(R.drawable.broken)
                    .into(view);
        }
    }

    @BindingAdapter({"imageUrlCircular"})
    public static void loadImageCircular(ImageView view, String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            Picasso.with(view.getContext())
                    .load(imageUrl)
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.broken)
                    .transform(CIRCLE_TRANSFORM)
                    .into(view);
        } else {
            Picasso.with(view.getContext())
                    .load(R.drawable.broken)
                    .into(view);
        }
    }

    public OnClickListener getListener() {
        return listener;
    }

    public void setListener(OnClickListener listener) {
        this.listener = listener;
    }

    public void clicked(View view) {
        if (listener != null) {
            listener.onClicked(view, this);
        }
    }

    public interface OnClickListener {
        //TODO use generics to make onclick take this class type as parameter
        void onClicked(View view, BasicViewModel listingViewModel);
    }

    public static class CircleTransform implements Transformation {

        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }
}
