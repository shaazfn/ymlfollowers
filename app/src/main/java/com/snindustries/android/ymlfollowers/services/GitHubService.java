package com.snindustries.android.ymlfollowers.services;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by u1cc16 on 3/10/2017.
 */
public class GitHubService extends IntentService {

	protected abstract class AsyncHttpTask extends AsyncTask<String, Void, String> {

		private final ResultReceiver receiver;
		private int status = -1;

		public AsyncHttpTask(ResultReceiver receiver) {
			this.receiver = receiver;
		}

		@Override
		protected String doInBackground(String... params) {
			StringBuilder result = new StringBuilder();

			try {
				URL url = getUrl(params[0]);
				URLConnection urlConnection = url.openConnection();
				InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
				String line;
				while ((line = reader.readLine()) != null) {
					result.append(line);
				}
				status = STATUS_FINISHED;
			} catch (IOException e) {
				e.printStackTrace();
				status = STATUS_ERROR;
			}

			return result.toString();
		}

		@NonNull
		abstract protected URL getUrl(String param) throws MalformedURLException;

		@Override
		protected void onPostExecute(String result) {
			Log.v(TAG, result);
			status = validateResult(result, status);

			Bundle bundle = new Bundle();
			bundle.putString(Intent.EXTRA_TEXT, result);

			receiver.send(status, bundle);
			GitHubService.this.stopSelf();
		}

		@Override
		protected void onPreExecute() {
			status = STATUS_STARTED;
			receiver.send(status, Bundle.EMPTY);
		}

		protected abstract int validateResult(String result, int status);
	}

	protected class UserFollowersTask extends AsyncHttpTask {

		public UserFollowersTask(ResultReceiver receiver) {
			super(receiver);
		}

		@NonNull
		@Override
		protected URL getUrl(String param) throws MalformedURLException {
			return new URL(USER_PROFILE_BASE_URL + param + "/followers");
		}

		@Override
		protected int validateResult(String result, int status) {
			//Quick and dirty validation
			if (result.contains("\"message\": \"Not Found\"")) {
				return STATUS_ERROR;
			}
			return STATUS_FINISHED_USER_FOLLOWERS;
		}
	}

	protected class UserProfileTask extends AsyncHttpTask {

		public UserProfileTask(ResultReceiver receiver) {
			super(receiver);
		}

		@NonNull
		@Override
		protected URL getUrl(String param) throws MalformedURLException {
			return new URL(USER_PROFILE_BASE_URL + param);
		}

		@Override
		protected int validateResult(String result, int status) {
			//Quick and dirty validation
			if (result.contains("\"message\": \"Not Found\"")) {
				return STATUS_ERROR;
			}
			return STATUS_FINISHED_USER_PROFILE;
		}
	}

	public static final String ENDPOINT_USER_FOLLOWERS = "ENDPOINT_USER_FOLLOWERS";
	public static final String ENDPOINT_USER_PROFILE = "ENDPOINT_USER_PROFILE";
	public static final String EXTRA_RESULT_RECEIVER = "GitHubService.RECEIVER";
	public static final String EXTRA_SERVICE_ENDPOINT = "GitHubService.SERVICE_ENDPOINT";
	public static final String EXTRA_USER_ID = "GitHubService.EXTRA_USER_ID";
	public static final int STATUS_ERROR = 2;
	public static final int STATUS_FINISHED = 1;
	public static final int STATUS_FINISHED_USER_FOLLOWERS = 4;
	public static final int STATUS_FINISHED_USER_PROFILE = 3;
	public static final int STATUS_STARTED = 0;
	public static final String TAG = "UPS";
	private static final String USER_PROFILE_BASE_URL = "https://api.github.com/users/";

	/**
	 * Creates an IntentService.  Invoked by your subclass's constructor.
	 */
	public GitHubService() {
		super(GitHubService.class.getName());
	}

	private void getUserFollowers(String user, ResultReceiver receiver) {
		new UserFollowersTask(receiver).execute(user);//TODO threadpool executor
	}

	private void getUserProfile(String user, ResultReceiver receiver) {
		new UserProfileTask(receiver).execute(user);//TODO threadpool executor
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent != null) {
			String user = intent.getStringExtra(EXTRA_USER_ID);
			ResultReceiver receiver = intent.getParcelableExtra(EXTRA_RESULT_RECEIVER);
			String serviceEndpoint = intent.getStringExtra(EXTRA_SERVICE_ENDPOINT);

			if (user != null && receiver != null && serviceEndpoint != null) {
				if (serviceEndpoint.equals(ENDPOINT_USER_PROFILE)) {
					getUserProfile(user, receiver);
				} else if (serviceEndpoint.equals(ENDPOINT_USER_FOLLOWERS)) {
					getUserFollowers(user, receiver);
				} else {
					//TODO Make proper exceptions
					//Quick and dirty exception for unsupported operation
					throw new UnsupportedOperationException("Endpoint not supported " + serviceEndpoint);
				}
			} else {
				//Quick and dirty exception for unsupported operation
				throw new UnsupportedOperationException("User or Receiver or serviceEndpoint is null");
			}
		}
	}

}
