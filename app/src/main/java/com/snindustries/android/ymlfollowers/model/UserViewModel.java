package com.snindustries.android.ymlfollowers.model;

import android.databinding.Bindable;

import com.snindustries.android.ymlfollowers.presentation.BasicViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * View Model class for Users
 *
 * Created by shaaz on 3/10/2017.
 */
public class UserViewModel extends BasicViewModel implements Serializable {
    //TODO: change the class to Pareclable for performance if required

    private String name;
    private String location;
    private String email;
    private int public_repository;
    private int followers;
    private int following;
    private String avatarUrl;
    private String login;

    public UserViewModel() {
        super(null);
        avatarUrl = "";
        email = "";
        followers = 0;
        following = 0;
        location = "";
        login = "";
        name = "";
        public_repository = 0;
    }

    public UserViewModel(OnClickListener listener, String json) throws JSONException {
        this(listener, new JSONObject(json));
    }

    public UserViewModel(OnClickListener listener, JSONObject json) throws JSONException {
        super(listener);
        this.login = json.getString("login");
        this.avatarUrl = json.getString("avatar_url");
        this.name = json.getString("name");
        this.location = json.getString("location");
        this.email = json.getString("email");
        this.public_repository = json.getInt("public_repos");
        this.followers = json.getInt("followers");
        this.following = json.getInt("following");
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getEmail() {
        return email;
    }

    public int getPublic_repository() {
        return public_repository;
    }

    public int getFollowers() {
        return followers;
    }

    public int getFollowing() {
        return following;
    }

    @Bindable
    public String getAvatarUrl() {
        return avatarUrl;
    }


    @Bindable
    public String getLogin() {
        return login;
    }

}
